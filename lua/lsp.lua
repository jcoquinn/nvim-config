-- vim.lsp.set_log_level("debug")

vim.api.nvim_create_user_command("LspStop", function(opts)
    vim.lsp.stop_client(vim.lsp.get_active_clients())
end, {})

vim.keymap.set("n", "[d", vim.diagnostic.goto_prev)
vim.keymap.set("n", "]d", vim.diagnostic.goto_next)
vim.keymap.set("n", "<space>e", vim.diagnostic.open_float)
vim.keymap.set("n", "<space>q", vim.diagnostic.setloclist)

local lsp_aug = vim.api.nvim_create_augroup("_lsp", {})

vim.api.nvim_create_autocmd("BufWritePre", {
    group = lsp_aug,
    pattern = { "*.go", "*.py" },
    callback = function(ev)
        vim.lsp.buf.format()
    end,
})

vim.api.nvim_create_autocmd("LspAttach", {
    group = lsp_aug,
    callback = function(ev)
        vim.bo[ev.buf].omnifunc = "v:lua.vim.lsp.omnifunc"

        local opts = { buffer = ev.buf, noremap = true }
        vim.keymap.set("n", "gd", vim.lsp.buf.definition, opts)
        vim.keymap.set("n", "gf", vim.lsp.buf.format, opts)
        vim.keymap.set("n", "gh", vim.lsp.buf.hover, opts)
        vim.keymap.set("n", "gi", vim.lsp.buf.implementation, opts)
        vim.keymap.set("n", "gr", vim.lsp.buf.references, opts)
        vim.keymap.set("n", "gs", vim.lsp.buf.signature_help, opts)
        -- vim.keymap.set("n", "", vim.lsp.buf.code_action, opts)
        -- vim.keymap.set("n", "", vim.lsp.buf.declaration, opts)
        -- vim.keymap.set("n", "", vim.lsp.buf.rename, opts)
        -- vim.keymap.set("n", "", vim.lsp.buf.type_definition, opts)
        -- vim.keymap.set("n", "", vim.lsp.buf.add_workspace_folder, opts)
        -- vim.keymap.set("n", "", vim.lsp.buf.remove_workspace_folder, opts)
        -- vim.keymap.set("n", "", function()
        --     print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
        -- end, opts)
    end,
})

local border = "single"
vim.diagnostic.config({ float = { border = border } })
vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, { border = border })
vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers.signature_help, { border = border })
