return {
    -- { "chaoren/vim-wordmotion" },
    {
        "rrethy/nvim-base16",
        lazy = false,
        priority = 1000,
        config = function()
            vim.cmd.colorscheme("base16-tomorrow-night")
            vim.api.nvim_set_hl(0, "LineNr", { fg = "#606060" })
        end,
    },
    { "mhinz/vim-signify" },
    { "numToStr/Comment.nvim" },
    { "vladdoster/remember.nvim", config = true },
    { "chaoren/vim-wordmotion" },
    {
        "neovim/nvim-lspconfig",
        config = function()
            local lspconfig = require("lspconfig")
            -- Golang
            lspconfig.gopls.setup({
                settings = {
                    gopls = {
                        -- gofumpt = true,
                        staticcheck = true,
                    },
                },
            })
            -- Python
            local function mypy_args()
                local prefix = os.getenv("VIRTUAL_ENV") or "/usr"
                return { "--python-executable", prefix .. "/bin/python", true }
            end
            lspconfig.pylsp.setup({
                settings = {
                    pylsp = {
                        plugins = {
                            black = { enabled = true },
                            flake8 = { enabled = true },
                            isort = { enabled = true, profile = "black" },
                            pylsp_mypy = {
                                enabled = true,
                                dmypy = true,
                                live_mode = false,
                                report_progress = true,
                                overrides = mypy_args(),
                            },
                            -- disabled
                            autopep8 = { enabled = false },
                            mccabe = { enabled = false },
                            pycodestyle = { enabled = false },
                            pydocstyle = { enabled = false },
                            pyflakes = { enabled = false },
                            pylint = { enabled = false },
                            rope_completion = { enabled = false },
                            yapf = { enabled = false },
                        },
                    },
                },
            })
            require("lspconfig.ui.windows").default_options.border = "single"
        end,
    },
    {
        "nvim-treesitter/nvim-treesitter",
        build = ":TSUpdate",
        config = function()
            require("nvim-treesitter.configs").setup({
                ensure_installed = { "go", "lua", "python" },
                auto_install = false,
                highlight = {
                    enable = true,
                },
            })
        end,
    },
    {
        "nvim-telescope/telescope.nvim",
        branch = "0.1.x",
        dependencies = { "nvim-lua/plenary.nvim" },
        opts = {
            pickers = {
                find_files = {
                    theme = "dropdown",
                },
                live_grep = {
                    theme = "dropdown",
                },
            },
        },
        config = function()
            local builtin = require("telescope.builtin")
            vim.keymap.set("n", "<leader>fb", builtin.buffers)
            vim.keymap.set("n", "<leader>ff", builtin.find_files)
            vim.keymap.set("n", "<leader>fo", builtin.treesitter)
            vim.keymap.set("n", "<leader>fr", builtin.live_grep)
            vim.keymap.set("n", "<leader>fz", builtin.current_buffer_fuzzy_find)
        end,
    },
    {
        "nvim-tree/nvim-tree.lua",
        dependencies = { "nvim-tree/nvim-web-devicons" },
        version = "*",
        lazy = false,
        opts = {
            filters = {
                dotfiles = true,
            },
        },
        config = function()
            require("nvim-tree").setup()
            vim.keymap.set("n", "<Leader>tt", ":NvimTreeToggle<CR>")
            vim.keymap.set("n", "<Leader>tf", ":NvimTreeFocus<CR>")
        end,
    },
    {
        "akinsho/bufferline.nvim",
        version = "*",
        dependencies = { "nvim-tree/nvim-web-devicons" },
        opts = {
            options = {
                mode = "buffers",
                numbers = "none",
                offsets = {
                    { filetype = "NvimTree" },
                },
            },
            highlights = {
                buffer_selected = {
                    italic = false,
                },
                indicator_selected = {
                    fg = { attribute = "fg", highlight = "Function" },
                    italic = false,
                },
            },
        },
    },
    {
        "nvim-lualine/lualine.nvim",
        dependencies = { "nvim-tree/nvim-web-devicons" },
        opts = {
            options = {
                theme = "base16",
                icons_enabled = false,
                component_separators = "|",
                section_separators = "",
                disabled_filetypes = {
                    statusline = { "NvimTree" },
                },
            },
        },
    },
    {
        "stevearc/conform.nvim",
        event = { "BufWritePre" },
        opts = {
            formatters_by_ft = {
                css = { "prettier" },
                html = { "prettier" },
                json = { "prettier" },
                javascript = { "prettier" },
                go = { "goimports", "golines", "gofumpt" },
                lua = { "stylua" },
                markdown = { "prettier" },
                python = { "isort", "black" },
                yaml = { "prettier" },
            },
            formatters = {
                goimports = {
                    append_args = { "-local", "app.local" },
                },
            },
            format_on_save = function(bufnr)
                if vim.g.disable_autoformat or vim.b[bufnr].disable_autoformat then
                    return
                end
                return { timeout_ms = 500, lsp_format = "fallback" }
            end,
        },
    },
    {
        "mfussenegger/nvim-lint",
        event = { "BufNewFile", "BufReadPre" },
        config = function()
            require("lint").linters_by_ft = {
                php = { "php" },
                yaml = { "yamllint" },
            }
            vim.api.nvim_create_autocmd({ "BufReadPost", "BufWritePost" }, {
                group = lint_aug,
                callback = function()
                    require("lint").try_lint()
                end,
            })
        end,
    },
}
