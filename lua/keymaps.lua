-- Esc
vim.keymap.set("i", "jk", "<Esc>")
vim.keymap.set("v", "<Leader>jk", "<Esc>")
vim.keymap.set("n", "q", "<Esc>")

-- Misc
vim.keymap.set("n", "H", "^")
vim.keymap.set("n", "L", "$")
vim.keymap.set("n", "ZQ", ":qa!<CR>")
vim.keymap.set("n", "<C-s>", ":w<CR>")
vim.keymap.set("n", "<C-q>", ":q<CR>")

-- buffers
vim.keymap.set("n", "M", ":bnext!<CR>", { silent = true })
vim.keymap.set("n", "N", ":bprev!<CR>", { silent = true })
vim.keymap.set("n", "<Leader>b", ":buffers<CR>:b", { silent = true })

-- windows
vim.keymap.set("n", "<C-h>", "<C-w>h")
vim.keymap.set("n", "<C-j>", "<C-w>j")
vim.keymap.set("n", "<C-k>", "<C-w>k")
vim.keymap.set("n", "<C-l>", "<C-w>l")
